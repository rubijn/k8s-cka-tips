# k8s-cka-tips

## Configure env 

```bash
alias k=kubectl                         
export do="--dry-run=client -o yaml"    
export now="--force --grace-period 0"   
alias kswitch="kubectl config set-context $(kubectl config current-context) "
```


```bash
~/.vimrc
set tabstop=2
set expandtab
set shiftwidth=2
```

## Links

Examples YAML
* https://k8s-examples.container-solutions.com/

Official API doc 
* [Node](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.29/#nodespec-v1-core)
* [Deployment](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.29/#deployment-v1-apps)
* [ReplicaSet](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.29/#replicaset-v1-apps)
* [Pod](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.29/#pod-v1-core)
* [PodSpec](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.29/#podspec-v1-core)
* [Container](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.29/)

